package lifesbest23.flappybird;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.ImageIcon;

//Add animations for Buttons
//Add animations in screen change
//add logo and lifesbest23
//add saving of HighScore
//add splash at dead

//		add login to a highscoreList

public class Game {

	public static int MAX_X = 288;
	public static int MAX_Y = 512;
	public static int GAME_BOTTOM = 400;
	
	public List<Pipe> pipes = new ArrayList<Pipe>();
	public Bird bird = new Bird(this);
	
	private int score = 0;
	private int highScore = 0;	
	
	private Image bg, bottom;
	private Image getReadyImage, getReadyText;
	private Image scoreBoard, playButton, topButton, gameOverText;
	private Image flappyBird, newImage;
	private Image platin, gold, silver, bronce;
	private Image[] bigNumbers = new Image[10];
	private Image[] smallNumbers = new Image[10];
	private int bottom_x = 0;
	
	private boolean newHighScore = false;
	
	public boolean running = false;
	public boolean gameOver = false;
	public boolean startUpStartScreen = true;
	public boolean getReadyScreen = true;
	public boolean gameOverFlash = false;
	
	public Game(){
		initImage();
	}
	
	private void initImage(){
		setBackGround();
		ImageIcon im = new ImageIcon(getClass().getResource("/resources/images/bottom.png"));
		bottom = im.getImage();
		im = new ImageIcon(getClass().getResource("/resources/images/getReady.png"));
		getReadyText = im.getImage();
		im = new ImageIcon(getClass().getResource("/resources/images/startScreen.png"));
		getReadyImage = im.getImage();
		im = new ImageIcon(getClass().getResource("/resources/images/scoreBoard.png"));
		scoreBoard = im.getImage();
		im = new ImageIcon(getClass().getResource("/resources/images/playButton.png"));
		playButton = im.getImage();
		im = new ImageIcon(getClass().getResource("/resources/images/topButton.png"));
		topButton = im.getImage();
		im = new ImageIcon(getClass().getResource("/resources/images/gameOver.png"));
		gameOverText = im.getImage();
		im = new ImageIcon(getClass().getResource("/resources/images/flappyBirds.png"));
		flappyBird = im.getImage();
		im = new ImageIcon(getClass().getResource("/resources/images/new.png"));
		newImage = im.getImage();
		im = new ImageIcon(getClass().getResource("/resources/images/bronce.png"));
		bronce = im.getImage();
		im = new ImageIcon(getClass().getResource("/resources/images/silver.png"));
		silver = im.getImage();
		im = new ImageIcon(getClass().getResource("/resources/images/gold.png"));
		gold = im.getImage();
		im = new ImageIcon(getClass().getResource("/resources/images/platin.png"));
		platin = im.getImage();
		
		for(int i = 0; i < 10; i++){
			im = new ImageIcon(getClass().getResource("/resources/images/big" + i + ".png"));
			bigNumbers[i] = im.getImage();
			im = new ImageIcon(getClass().getResource("/resources/images/small" + i + ".png"));
			smallNumbers[i] = im.getImage();
		}
	}
	
	private void setBackGround(){
		Random r = new Random();
		int k = r.nextInt(2) + 1;
		ImageIcon im = new ImageIcon(getClass().getResource("/resources/images/bg" + k + ".png"));
		bg = im.getImage();
	}
	
	public void restart(){
		pipes = new ArrayList<Pipe>();

		running = true;
		gameOver = false;
		startUpStartScreen = false;
		getReadyScreen = false;
		gameOverFlash = false;
		
		score = 0;
		
		bird.reset();
		bird.jump();
	}
	
	public void draw(Graphics2D g){
		//draw Background
		g.drawImage(bg, 0, 0, null);
		
		//drawPipes
		for(int i = 0; i < pipes.size(); i++)
			pipes.get(i).draw(g);
		//draw Bird
		bird.draw(g);
		
		//draw Score
		if(!gameOver && !startUpStartScreen)
			drawScore(g);
		
		if(startUpStartScreen)
			drawStartUpStartScreen(g);
		else if(getReadyScreen)
			drawGetReadyScreen(g);
		else if(gameOver)
			drawGameOverScreen(g);
		
		//draw the bottom
		g.drawImage(bottom, bottom_x, GAME_BOTTOM, null);
		
		//1		draw Message -> before JFrame
		//2		draw Game
		//3		draw StartUp start screen (Play, 123, flappy bird)
		//4		draw get Ready Screen
		//5		draw Splash Game Over
		//6		draw Game Over screen (Play, 123, Game Over, Medal) 
		//		|->		4
	}
	
	public void space(){
		if(running)
			bird.jump();
//		else if(gameOver)
//			onGetReadyScreen();
		else if(startUpStartScreen)
			onGetReadyScreen();
		else if(getReadyScreen)
			restart();
	}
	
	public void enter(){
		if(gameOver)
			onGetReadyScreen();
	}
	
	public void click(Point2D p){
		int y = scoreBoard.getHeight(null) + 170;
		int x = (MAX_X - playButton.getWidth(null) - topButton.getWidth(null) - 20)/2;
		
		Rectangle2D play = new Rectangle2D.Double(x, y,
				playButton.getWidth(null), playButton.getHeight(null));
		Rectangle2D top = new Rectangle2D.Double(x + playButton.getWidth(null) + 20, y,
				playButton.getWidth(null), playButton.getHeight(null));
		
		if(gameOver || startUpStartScreen){
			if(play.contains(p))
				onGetReadyScreen();
			else if(top.contains(p))
				showTop();
		}
	}
		
	private void showTop(){
		
	}
	
	private void onGetReadyScreen(){
		Sound.playSwooshing();
		pipes = new ArrayList<Pipe>();

		if(!startUpStartScreen)
			setBackGround();
		
		running = false;
		gameOver = false;
		startUpStartScreen = false;
		getReadyScreen = true;
		gameOverFlash = false;
		
		newHighScore = false;
		
		score = 0;
		
		bird.reset();
	}
	
	private void drawStartUpStartScreen(Graphics2D g){
		g.drawImage(flappyBird, MAX_X/2 - flappyBird.getWidth(null)/2, 50, null);
		
		int y = scoreBoard.getHeight(null) + 170;
		int x = (MAX_X - playButton.getWidth(null) - topButton.getWidth(null) - 20)/2;
		g.drawImage(playButton, x, y, null);
		g.drawImage(topButton, x + playButton.getWidth(null) + 20, y, null);
	}
	
	private void drawGetReadyScreen(Graphics2D g){
		g.drawImage(getReadyText, MAX_X/2 - getReadyText.getWidth(null)/2, 50, null);
		g.drawImage(getReadyImage, MAX_X/2 - getReadyImage.getWidth(null)/2, 200, null);
	}
	
	private void drawGameOverScreen(Graphics2D g){
		g.drawImage(gameOverText, MAX_X/2 - gameOverText.getWidth(null)/2, 50, null);
		BufferedImage im = new BufferedImage(scoreBoard.getWidth(null), scoreBoard.getHeight(null),
				BufferedImage.TYPE_4BYTE_ABGR);
		Graphics2D g2 = (Graphics2D) im.getGraphics();
		g2.drawImage(scoreBoard, 0, 0, null);
		//draw Score and Medals and maybe new
		
		//new score
		String s = Integer.toString(score);
		int digits = s.length();
		Image ret = new BufferedImage(1, 20, BufferedImage.TYPE_4BYTE_ABGR);
		for(int i = 0; i < digits; i++){
			Image im2 = smallNumbers[Integer.parseInt(new String(new char[]{s.charAt(i)}))];
			BufferedImage retn = new BufferedImage(ret.getWidth(null) + im2.getWidth(null) - 2, 36, 
					BufferedImage.TYPE_4BYTE_ABGR);
			retn.getGraphics().drawImage(ret, 0, 0, null);
			retn.getGraphics().drawImage(im2, ret.getWidth(null) - 2, 0, null);
			ret = retn;
		}
		g2.drawImage(ret, 203 - ret.getWidth(null), 35, null);
		
		//Determine if it is a new highscore
		if(score > highScore){
			highScore = score;
			newHighScore = true;
		}
		//draw new sign
		if(newHighScore)
			g2.drawImage(newImage, 120, 58, null);
		
		//draw highScore
		s = Integer.toString(highScore);
		digits = s.length();
		ret = new BufferedImage(1, 20, BufferedImage.TYPE_4BYTE_ABGR);
		for(int i = 0; i < digits; i++){
			Image im2 = smallNumbers[Integer.parseInt(new String(new char[]{s.charAt(i)}))];
			BufferedImage retn = new BufferedImage(ret.getWidth(null) + im2.getWidth(null) - 2, 36, 
					BufferedImage.TYPE_4BYTE_ABGR);
			retn.getGraphics().drawImage(ret, 0, 0, null);
			retn.getGraphics().drawImage(im2, ret.getWidth(null) - 2, 0, null);
			ret = retn;
		}
		g2.drawImage(ret, 203 - ret.getWidth(null), 75, null);
		
		//draw the medals
		switch(score/10){
		case 1:
			g2.drawImage(bronce, 26, 42, null);
			break;
		case 2:
			g2.drawImage(silver, 26, 42, null);
			break;
		case 3:
			g2.drawImage(gold, 26, 42, null);
			break;
		case 4:
			g2.drawImage(platin, 26, 42, null);
			break;
		}
		//draw the scoreBoard
		g.drawImage(im, MAX_X/2 - im.getWidth()/2, 150, null);
		
		int y = scoreBoard.getHeight(null) + 170;
		int x = (MAX_X - playButton.getWidth(null) - topButton.getWidth(null) - 20)/2;
		g.drawImage(playButton, x, y, null);
		g.drawImage(topButton, x + playButton.getWidth(null) + 20, y, null);
	}
	
	private void drawScore(Graphics2D g){
		String s = Integer.toString(score);
		int digits = s.length();
		Image ret = new BufferedImage(1, 36, BufferedImage.TYPE_4BYTE_ABGR);
		for(int i = 0; i < digits; i++){
			Image im = bigNumbers[Integer.parseInt(new String(new char[]{s.charAt(i)}))];
			BufferedImage retn = new BufferedImage(ret.getWidth(null) + im.getWidth(null) - 2, 36, 
					BufferedImage.TYPE_4BYTE_ABGR);
			retn.getGraphics().drawImage(ret, 0, 0, null);
			retn.getGraphics().drawImage(im, ret.getWidth(null) - 2, 0, null);
			ret = retn;
		}
		g.drawImage(ret, MAX_X / 2 - ret.getWidth(null) / 2, 100, null);
	}
	
	public void move(){
		//Move the bird if GameOver
		if(gameOver)
			bird.move();
		
		else if(startUpStartScreen)
			bird.moveStartUpStartScreen();
		
		else if(getReadyScreen)
			bird.moveGetReadyScreen();
		
		//Whenever it is not game Over move bottom
		if(!gameOver){
			bottom_x -= 2;
			if(bottom_x < -23)
				bottom_x = 0;
		}
		if(running){
			//movePipes
			for(int i = 0; i < pipes.size(); i++)
				pipes.get(i).move();
			//if there are pipes and last pipe is more than 150 px away from the rigth corner
			if(pipes.size() > 0 && pipes.get(pipes.size() - 1).x - Game.MAX_X < -150)
				pipes.add(Pipe.newPipe());
			//else if there is no pipe
			else if(pipes.size() < 1)
				pipes.add(Pipe.newPipe());
			
			//if the first pipe is out of sight remove
			if(pipes.size() > 0 && pipes.get(0).x + Pipe.WIDTH < 0)
				pipes.remove(0);
			
			//move Bird
			bird.move();
			
			//check the collisions
			checkCollision();
		}
	}
	
	private void checkCollision(){
		for(int i = 0; i < pipes.size(); i++)
			if(pipes.get(i).getBoundsUp().intersects(bird.getBounds()) ||
					pipes.get(i).getBoundsDown().intersects(bird.getBounds())){
				running = false;
				gameOver = true;
				Sound.playHit();
				Sound.playDie();
			}
		
		if(bird.getBounds().getMaxY() > Game.GAME_BOTTOM){
			running = false;
			gameOver = true;
			Sound.playHit();
			Sound.playDie();
		}
		
		if(pipes.get(0).x + Pipe.WIDTH/2 + 1 == bird.getBounds().getMaxX()){
			score++;
			Sound.playPoint();
		}
		
	}
}
