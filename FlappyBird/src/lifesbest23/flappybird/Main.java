package lifesbest23.flappybird;

import javax.swing.JFrame;

//Add a splash Screen at the start for a few milliseconds

public class Main extends JFrame{
	private static final long serialVersionUID = 3831317933579507326L;

	public Main(){
		super();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("FlappyBird");
		
		this.add(new Display());
		
		this.pack();
		
		this.setResizable(false);
		this.setVisible(true);
	}
	
	public static void main(String[] str){
		new Main();
	}

}
