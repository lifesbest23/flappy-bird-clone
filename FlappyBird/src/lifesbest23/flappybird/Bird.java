package lifesbest23.flappybird;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;

import javax.swing.ImageIcon;

public class Bird {
	
	public static final int X_PLAY = 40;
	
	public int x;
	public int y;
	
	private Game game;
	
	private int g = 40;
	private int vy = 0;
	private double jump = -620;
	public float rot = 0;
	
	private Image[] image;
	private int animation = 1;
	private int animationD = 1;
	private int animationCount = 0;
	
	public Bird(Game g){
		y = Game.GAME_BOTTOM/2*100;
		x = X_PLAY;
		
		game = g;
		
		initImage();
	}
	
	private void initImage(){
		image = new Image[3];
		for(int i = 0; i < 3; i++){
			ImageIcon im = new ImageIcon(getClass().getResource("/resources/images/bird1" + i + ".png"));
			image[i] = im.getImage();
		}
	}
	
	public void move(){
		if(getBounds().getMaxY() > Game.GAME_BOTTOM)
			return;

		if( getBounds().getMaxY() > 0 || vy > 0)
			y += vy;
		vy += g;
		
		animate();
	}
	
	private void animate(){
		if(animationCount++ < 2)
			return;
		
		animationCount = 0;
		
		if(animation == 0){
			animation = 1;
			animationD = -animationD;
		}
		else if(animation == 1)
			animation += animationD;
		else if(animation == 2){
			animation = 1;
			animationD = -animationD;
		}
	}
	
	public void moveStartUpStartScreen(){
		x = Game.MAX_X / 2 - image[0].getWidth(null)/2;

		if(animationCount < 2)
			y += animationD*100;
		
		animate();
	}
	
	public void moveGetReadyScreen(){
		x = X_PLAY;

		if(animationCount < 2)
			y += animationD*100;
		
		animate();
	}
	
	public Rectangle2D getBounds(){
		Rectangle2D ret = new Rectangle2D.Double(x + 3, y/100 + 3,
				image[0].getWidth(null) - 6, image[0].getHeight(null) - 6);
		return ret;
	}
	
	public void jump(){
		if(game.running){
			vy = (int) jump;
			Sound.playFlap();
		}
	}
	
	public void reset(){
		y = Game.GAME_BOTTOM/2*100;
		x = X_PLAY;
		vy = 0;
	}
	
	public void draw(Graphics2D g){
		AffineTransform at = AffineTransform.getTranslateInstance(x + image[0].getWidth(null)/2, 
				y/100 + image[0].getHeight(null)/2);
		at.rotate(-(Math.PI)*((vy/this.g)/(jump))*7);
		g.setTransform(at);
		g.drawImage(image[animation], - image[0].getWidth(null)/2, - image[0].getHeight(null)/2, null);
		g.setTransform(new AffineTransform());
	}

}
