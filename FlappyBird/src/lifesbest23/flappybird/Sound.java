package lifesbest23.flappybird;

import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class Sound {
	
	public static void playFlap(){
		new Thread(){
			public void run(){
				AudioInputStream musicStream = null;
				try {
					musicStream = AudioSystem.getAudioInputStream(
							getClass().getResource("/resources/sound/wing.wav"));
				} catch (UnsupportedAudioFileException e) {e.printStackTrace();
				} catch (IOException e) {e.printStackTrace();}
				
				Clip c = null;
				
				try {
					c = AudioSystem.getClip();
				} catch (LineUnavailableException e1) {e1.printStackTrace();}
				try {
					c.open(musicStream);
				} catch (LineUnavailableException e) {e.printStackTrace();
				} catch (IOException e) {e.printStackTrace();}
				
				c.start();
			}
		}.start();
	}
	
	public static void playSwooshing(){
		new Thread(){
			public void run(){
				AudioInputStream musicStream = null;
				try {
					musicStream = AudioSystem.getAudioInputStream(
							getClass().getResource("/resources/sound/swooshing.wav"));
				} catch (UnsupportedAudioFileException e) {e.printStackTrace();
				} catch (IOException e) {e.printStackTrace();}
				
				Clip c = null;
				
				try {
					c = AudioSystem.getClip();
				} catch (LineUnavailableException e1) {e1.printStackTrace();}
				try {
					c.open(musicStream);
				} catch (LineUnavailableException e) {e.printStackTrace();
				} catch (IOException e) {e.printStackTrace();}
				
				c.start();
			}
		}.start();
	}
	
	public static void playPoint(){
		new Thread(){
			public void run(){
				AudioInputStream musicStream = null;
				try {
					musicStream = AudioSystem.getAudioInputStream(
							getClass().getResource("/resources/sound/point.wav"));
				} catch (UnsupportedAudioFileException e) {e.printStackTrace();
				} catch (IOException e) {e.printStackTrace();}
				
				Clip c = null;
				
				try {
					c = AudioSystem.getClip();
				} catch (LineUnavailableException e1) {e1.printStackTrace();}
				try {
					c.open(musicStream);
				} catch (LineUnavailableException e) {e.printStackTrace();
				} catch (IOException e) {e.printStackTrace();}
				
				c.start();
			}
		}.start();
	}
	
	public static void playHit(){
		new Thread(){
			public void run(){
				AudioInputStream musicStream = null;
				try {
					musicStream = AudioSystem.getAudioInputStream(
							getClass().getResource("/resources/sound/hit.wav"));
				} catch (UnsupportedAudioFileException e) {e.printStackTrace();
				} catch (IOException e) {e.printStackTrace();}
				
				Clip c = null;
				
				try {
					c = AudioSystem.getClip();
				} catch (LineUnavailableException e1) {e1.printStackTrace();}
				try {
					c.open(musicStream);
				} catch (LineUnavailableException e) {e.printStackTrace();
				} catch (IOException e) {e.printStackTrace();}
				
				c.start();
			}
		}.start();
	}
	
	public static void playDie(){
		new Thread(){
			public void run(){
				AudioInputStream musicStream = null;
				try {
					musicStream = AudioSystem.getAudioInputStream(
							getClass().getResource("/resources/sound/die.wav"));
				} catch (UnsupportedAudioFileException e) {e.printStackTrace();
				} catch (IOException e) {e.printStackTrace();}
				
				Clip c = null;
				
				try {
					c = AudioSystem.getClip();
				} catch (LineUnavailableException e1) {e1.printStackTrace();}
				try {
					c.open(musicStream);
				} catch (LineUnavailableException e) {e.printStackTrace();
				} catch (IOException e) {e.printStackTrace();}
				
				c.start();
			}
		}.start();
	}
}
