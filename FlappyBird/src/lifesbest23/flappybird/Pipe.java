package lifesbest23.flappybird;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.Rectangle2D;
import java.util.Random;

import javax.swing.ImageIcon;

public class Pipe {
	
	public int x;
	public int y;
	
	public static int WIDTH = 52;
	public static int GAP = 100;
	
	private Image imageUp, imageDown;
	
	private Pipe(int y){
		this.x = Game.MAX_X;
		this.y = y;
		
		initImage();
	}
	
	public void initImage(){
		ImageIcon im = new ImageIcon(getClass().getResource("/resources/images/pipeGreenUp.png"));
		imageUp = im.getImage();
		im = new ImageIcon(getClass().getResource("/resources/images/pipeGreenDown.png"));
		imageDown = im.getImage();
	}
	
	public Rectangle2D getBoundsUp(){
		return new Rectangle2D.Double(x, y + GAP, WIDTH, imageUp.getHeight(null));
	}
	
	public Rectangle2D getBoundsDown(){
		return new Rectangle2D.Double(x, y- imageDown.getHeight(null), WIDTH, imageDown.getHeight(null));
	}
	
	public static Pipe newPipe(){
		Random r = new Random();
		return new Pipe(r.nextInt(Game.GAME_BOTTOM - 80 - GAP ) + 40);
	}
	
	public static Pipe newPipe(int x, int y){
		Pipe ret = new Pipe(y);
		ret.x = x;
		return ret;
	}
	
	public void move(){
		x -= 2;
	}
	
	public void draw(Graphics2D g){
		g.drawImage(imageDown, x, y - imageDown.getHeight(null), null);
		g.drawImage(imageUp, x, y + GAP, null);
	}

}
