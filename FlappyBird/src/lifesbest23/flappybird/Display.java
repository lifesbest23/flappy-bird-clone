package lifesbest23.flappybird;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;

public class Display extends JPanel implements KeyListener, MouseListener{
	private static final long serialVersionUID = 2504546590328339155L;
	
	private Game game;
	private boolean space = false;
	
	public Display(){
		super();
		
		game = new Game();
		
		this.setPreferredSize(new Dimension(Game.MAX_X, Game.MAX_Y));
		this.setFocusable(true);
		this.addKeyListener(this);
		this.addMouseListener(this);
		
		new Thread(){
			public void run(){
				while(true){
					game.move();repaint();
					try {
						Thread.sleep(20);
					} catch (InterruptedException e) {e.printStackTrace();}
				}
			}
		}.start();
	}
	
	public void paint(Graphics g2){
		Graphics2D g = (Graphics2D) g2;
		
		super.paint(g);
		
		game.draw(g);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_SPACE && !space){
			game.space();
			space = true;
		}
		else if(e.getKeyCode() == KeyEvent.VK_R)
			game.restart();
		else if(e.getKeyCode() == KeyEvent.VK_ENTER)
			game.enter();
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_SPACE && space){
			space = false;
		}
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		game.click(e.getPoint());
	}

}
